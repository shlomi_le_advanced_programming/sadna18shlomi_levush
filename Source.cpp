
#include "Helper.h"
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <Windows.h>

int pwd() {
	TCHAR tmp[1024]; //or wchar_t * buffer;
	int check=::GetModuleFileNameA(NULL, tmp, 1024);
	if (check != 0) {
		std::cout << tmp<< std::endl;
	}
	return check;
}
int cd(std::vector<std::string> command) {
	LPCSTR path = command[1].c_str();
	if (command[1] == "") {
		return 0;
	}
	int check=::SetCurrentDirectoryA(path);
	if (check != 0) {
		std::cout << "Changed dir to: " << command[1]<< std::endl;
	}
	return check;
}
int create(std::vector<std::string> command) {
	::CreateFileA(command[1].c_str(), GENERIC_READ | GENERIC_WRITE,0,NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,NULL);
	std::cout << "Created file named " << command[1]<<std::endl;
	return 0;

}
int ls() {
	TCHAR tmp[1024]; //or wchar_t * buffer;
	int check = ::GetModuleFileNameA(NULL, tmp, 1024);
	std::string path = tmp;
}


int main() {
	std::string str;
	int checker = 0;
	std::vector<std::string> command;
	while (1) {
		std::getline(std::cin,str);
		if (str == "pwd") {
			checker=pwd();
			if (checker == 0) {
				return 1;//error
			}
			continue;
		}
		command = Helper::get_words(str);
		if (command[0] == "cd") {
			checker=cd(command);
			{
				if (checker == 0) {
					return 1;
				}
				continue;
			}
			
		}
		if (command[0] == "create") {
			checker = create(command);
			continue;
		}
		if (command[0] == "ls") {
			ls();
		}

	}
	return 0;
}